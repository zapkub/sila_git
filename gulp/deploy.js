var path = require('path');
var gulp = require('gulp');
var ftp = require('vinyl-ftp');
var gutil = require('gulp-util');
var minimist = require('minimist');

gulp.task('deploy', function() {
  console.log('deploy');
  var remotePath = '/public_html/dist/';
  var conn = ftp.create({
    host: 'ftp.rsilathip.com',
    port:21,
    user: 'rsilathipc',
    password: 'Sila1234',
    log: gutil.log
  });

  gulp.src(['./dist/**/*'])
    .pipe(conn.newer(remotePath))
    .pipe(conn.dest(remotePath));
});

gulp.task('deploy:patch', function() {
  var remotePath = '/public_html/dist/';
  var conn = ftp.create({
    host: 'ftp.rsilathip.com',
    port:21,
    user: 'rsilathipc',
    password: 'Sila1234',
    log: gutil.log
  });

  gulp.src(['./dist/**/*','!./dist/asset/**/*'])
    .pipe(conn.newer(remotePath))
    .pipe(conn.dest(remotePath));
});
