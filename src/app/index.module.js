/* global malarkey:false, moment:false Upload:false */

import { config } from './index.config';
import { routerConfig,resourceData } from './index.route';
import { runBlock } from './index.run';
import underscore from 'underscore';
import { FooterDirective } from '../app/components/footer/footer.directive';
import { HeaderDirective } from '../app/components/header/header.directive';
import { AboutUsController,GalleryController,TransitionDirective ,ServicesController,ProductsController,ContactsController } from './controller'

angular.module('sila', ['uiGmapgoogle-maps','ngAnimate', 'ngCookies', 'ngStorage','slick','ngTouch', 'ngSanitize','ngFileUpload', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'toastr','anim-in-out'])
  .constant('malarkey', malarkey)
  .constant('moment', moment)
  .constant('lang','th')
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .constant('_',underscore)
  .service('resource',resourceData)
  .directive('header',HeaderDirective)
  .directive('footer',FooterDirective)
  .directive('fadeOnScroll',TransitionDirective)
  // front
  .controller('ProductsController',ProductsController)
  .controller('GalleryController',GalleryController)
  .controller('AboutUsController',AboutUsController)
  .controller('ContactsController',ContactsController)
  .directive('onFinishRender', function ($timeout) {
      return {
          restrict: 'A',
          link: function (scope, element, attr) {
              if (scope.$last === true) {
                  $timeout(function () {
                      scope.$emit('ngRepeatFinished');
                  });
              }
          }
      }
  });
