export function HeaderDirective($window) {
  'ngInject';
  let directive = {
    restrict: "E",
    templateUrl: 'app/components/header/header.html',
    controller: headerController,
    link: (scope, elem) => {
        angular.element($window).bind('scroll',()=>{
          let val = $window.pageYOffset;
          if (val > 1) {
            angular.element(elem).addClass('toogle')
          } else {
            angular.element(elem).removeClass('toogle')
          }
        })
    },
    controllerAs: "header"
  }
  return directive;
}


class headerController {
  constructor($state, $window) {
    'ngInject';
    this.state = $state.current.name;
  }
}
