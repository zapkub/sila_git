export function FooterDirective(){
  let directive = {
    restrict:"E",
    templateUrl:"app/components/footer/footer.html"
  }
  return directive;
}
