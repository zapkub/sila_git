let baseURL = "http://www.rsilathip.com/api/"
export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
  .state('home',{
    url:'/',
    views:{
      main:{
        templateUrl:'app/aboutus/aboutus.html',
        controller:'AboutUsController',
        controllerAs:'ctrl'
      },
      menu:{
        template:'<header></header>'
      }
    },
    resolve:{
      data:($resource)=>{
        let getpic = $resource(baseURL+'getImages.php?target=slider');
        return getpic.get().$promise;
      }
    }
  })
  .state('products',{
    url:'/products',
    views:{
      main:{
        templateUrl:'app/products/products.html',
        controller:'ProductsController',
        controllerAs:'ctrl'
      },
      menu:{
        template:'<header></header>'
      }
    },
    resolve:{
      data:($resource)=>{
        let getpic = $resource(baseURL+'getImages.php');
        return getpic.get().$promise;
      }
    }
  })
  .state('products.all',{
    url:'/',
    templateUrl:'app/products/gallery.html',
    controller:"GalleryController",
    controllerAs:'ctrl',
    resolve:{
      data:($resource)=>{
        let getpic = $resource(baseURL+'getImages.php');
        return getpic.get().$promise;
      }
    }
  })
  .state('products.filter',{
    url:'/:gallery_id',
    templateUrl:'app/products/gallery.html',
    controller:"GalleryController",
    controllerAs:'ctrl'

  })
  .state('service',{
    url:'/service',
    views:{
      main:{
        templateUrl:'app/services/service.html'
      }
    }
  })
  .state('contact',{
    url:'/contact',
    views:{
      main:{
        templateUrl:'app/contact/contact.html',
        controller:"ContactsController",
        controllerAs:"ctrl"
      }
    }
  });

  $urlRouterProvider.otherwise('/');
}


export class resourceData {
  constructor($resource,  $log, $q, Upload) {
    'ngInject';
    this.$resource = $resource;
    this.baseUrl = 'http://www.rungsikorn.rocks:8081/';
    this.aboutus = $resource(this.baseUrl+"api/aboutus")
    this.posts = $resource(this.baseUrl+"api/product/:id", {
      id: "@id"
    }, {
      update: {
        method: 'PUT'
      }
    });
    this.gallery = $resource(this.baseUrl+'api/gallery/:id')

    this.$q = $q;
    this.Upload = Upload;
  }
  getAboutus(){
    return this.getQ(this.aboutus.get())
  }
  getAllProduct() {
    let deferred = this.$q.defer();


    this.posts.get({}).$promise.then(
      (res) => {

        deferred.resolve(res);
      },
      () => {

        deferred.reject({
          msg: 'error'
        })
      }
    )
    return deferred.promise;
  }
  getProduct(id) {
    let deferred = this.$q.defer();


    this.posts.get({
      id: id
    }).$promise.then(
      (res) => {

        deferred.resolve(res);
      },
      () => {

        deferred.reject({
          msg: 'error'
        })
      }
    )
    return deferred.promise;
  }
  updateProduct(id, product) {
    let deferred = this.$q.defer();
    this.posts.update({
      id: id
    }, product).$promise.then(
      (res) => {
        deferred.resolve(res);
      }
    )
    return deferred.promise;
  }
  addProduct(name, desc, price, images) {
    return this.Upload.upload({
        url: this.baseUrl+"api/product/",
        arrayKey: '',
        data: {
          files: images,
          name: name,
          price: price,
          description: desc
        }
      })
    }
    //end products
    // Gallery
  getGalleries(){

    return this.getQ(this.gallery.get({}))
  }
  addGallery(title,files){
    return this.Upload.upload({
        url: this.baseUrl+"api/gallery/",
        arrayKey: '',
        data: {
          files: files,
          name: title
        }
      })
    }

  getQ(resourceRequest){
    let deferred = this.$q.defer();


    resourceRequest.$promise.then(
      (res) => {

        deferred.resolve(res);
      },
      () => {

        deferred.reject({
          msg: 'error'
        })
      }
    )
    return deferred.promise;
  }
}
