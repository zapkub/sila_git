
export class AboutUsController {
  constructor(data,_) {
    'ngInject';
    this.data = data;
    this.slider = [];
    this.images = [];
    _.each(this.data.product,(item)=>{
      this.slider.push(item.path);
    })
    _.each(this.data.fb.photos.data, (item) => {
      this.images.push({
        title: item.name,
        url: item.images[0].source,
        link:item.link
      });
    })
  }
}




export class ServicesController {
  constructor(lang) {

  }
}
export class ProductsController {
  constructor($scope,data, _, $window, $log, $http, $resource) {
    'ngInject';
    this._ = _;
    this.log = $log.log;
    this.http = $http;
    this.data = data.product;
    this.window = $window;
    this.scope = $scope;
    this.client = $resource;
    //
    // $http.get('http://localhost/sila_git/php/getImages.php').then(
    //   (res)=>{
    //     this.log(res);
    //   },
    //   ()=>{
    //     this.log('error')
    //   }
    // )

    this.init();
  }
  init() {

  }
}
export class GalleryController extends ProductsController {

  init() {
    this.imageHeight = this.window.innerWidth / 3;
    this.images = [];
    this.showGallery = false;
    this.lastImageIndex = 0;
    this.setGalleryItem(this.data);
    this.scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
        //you also get the actual event object
        //do stuff, execute functions -- whatever...
      angular.element('#gallery-full-screen').slick({

        })
    });

  }
  setGalleryItem(images){
    this._.each(images, (item) => {
          this.images.push({
            title_en: item.title_en,
            title_th:item.title_th,
            desc_en:item.desc_en,
            desc_th:item.desc_th,
            date:item.date_created,
            url: item.path,
            link:item.link

          });
          this.lastImageIndex = item.id;
        });
  }
  toogleGallery(index){
    this.showGallery = true;
    angular.element('#gallery-full-screen').slick('slickGoTo',index)
  }
  getMore(){
    this.scope.loading = true;
    this.client('http://www.rsilathip.com/api/'+'getImages.php?start=' + this.lastImageIndex).get().$promise.then(
      (res) => {
        this.setGalleryItem(res.product);
        this.scope.loading = false;
        if(res.product.length == 0){
          this.scope.end = true;
        }
      }
    )
  }
}
export class ContactsController {
  constructor($scope, $compile, $log) {
    'ngInject';
      var mapCenter = {
        lat: 13.330933,
        lng: 100.938220
      }
      function initMap() {
        var mapDiv = document.getElementById('map');
        var map = new google.maps.Map(mapDiv, {
          center: mapCenter,
          zoom: 17,
          disableDefaultUI: true
        });
        var marker = new google.maps.Marker({
          position: {
            lat: 13.329133,
            lng: 100.938140
          },
          map: map
        });
        var infowindow = new google.maps.InfoWindow({
          content: `
            <div class="center-detail" id="detail">
              <div class="title">
                บริษัท รุ่งเรืองศิลาทิพย์ {{address}}
              </div>
              <p>
                  <i class="fi-marker"></i>
                    9/76 หมู่ 3 ตำบล เสม็ด อำเภอ เมือง จังหวัด ชลบุรี 20000
                    <br>
                    <i class="fi-marker"></i> 9/76 M.3 T.samed A.muang Chonburi 20000

                <br>
                  <i class="fi-telephone"></i> 089-938-6866
                <br>
                  <i class="fi-mail"></i> Silathip2013@gmail.com
              </p>
            </div>
            `
        });
        infowindow.open(map, marker);
        google.maps.event.addListener(infowindow, 'domready', function(){
          let detail = document.getElementById('detail');
          $log.log(detail);
          $compile(detail)($scope);
          $scope.$apply();
        })
        window.addEventListener("resize", function() {
          map.setCenter(mapCenter)
        });
      }
      initMap();

  }
}

export function TransitionDirective($window, $log) {
  'ngInject';
  let directive = {
    restrict: "A",
    scope: {

    },
    link: (scope, elem) => {
      let rect = elem.context.getBoundingClientRect();
      if ($window.innerHeight - rect.top >= rect.height / 2) {
        angular.element(elem).addClass('animate-in');
      }
      angular.element($window).bind('scroll', () => {
        let rect = elem.context.getBoundingClientRect();
        if ($window.innerHeight - rect.top >= rect.height / 2) {
          angular.element(elem).addClass('animate-in');
        }
      })
    }
  }
  return directive;
}
