export function runBlock($log, $rootScope, $localStorage,$window) {
  'ngInject';
  $log.debug('runBlock end');
  NProgress.configure({ showSpinner: false });
  NProgress.configure({ easing: 'ease', speed: 500 });
  $rootScope.$on('$stateChangeStart', ()=>{
    NProgress.start();
  })

  $rootScope.$on('$stateChangeSuccess',()=>{
      NProgress.done();
      $($window).scrollTop(0);
  })

  if (!$localStorage.lang) {
    $localStorage.lang = 'th';
  }
  $rootScope.$lang = $localStorage.lang;
  $rootScope.$watch('$lang', (val) => {
    $localStorage.lang = val;
  })
  $rootScope.$content = {
    company_title: {
      th: "บริษัท รุ่งเรืองศิลาทิพย์ จำกัด",
      en: "Rung Reang Silathip co."
    },
    slogan:{
      th:"ครกหินอ่างศิลา since 1957",
      en:"Original Angsila’s mortar since 1957"
    }  ,
    founder:{
      en:`
        Mr.Kajohn Rungreangsilathip,Who has been passed on the knowledge about stone caving from his father (Mr.Nginhai sae li), is continue teaching & training new generation sculptures to be expert in stone caving and also expanding and developing the stone business in Angsila to be not just a mortar&pestle but also a stone sculpture like Granite chinese lion sculpture, Granite elephant sculpture or many kind of decorative art sculptures.
      `,
      th: `
         นายขจร รุ่งเรืองศิลาทิพย์ ซึ่งเป็นผู้ที่ได้รับการถ่ายทอด ความรู้เรื่องหิน และการตีหิน มาจากบิดา (นายไฮ้ แซ่ลี้) ได้สืบสาน เผยแพร่และ ฝึกฝนช่างรุ่นใหม่ๆให้รู้จักวิธีการตีหิน อีกทั้งขยับขยาย พัฒนา อุตสาหกรรมเกี่ยวกับหิน ไม่เพียงแค่แกะสลักครกหิน แต่ได้มีการแกะสลักหินเป็นรูปทรงต่างๆ อีกหลากหลาย เช่น สิงโตหิน ช้างหิน พระหิน และหินตกแต่งสวนในรูปแบบต่างๆ เป็นต้น  ซึ่งเป็นการเพิ่มมูลค่า และสร้างอาชีพ สืบสาน และดำรงเอกลักษณ์ ของอ่างศิลาอีกด้วย

      `
    },
    wiseword: {
      th: `

        เริ่มต้นธุรกิจด้วย <span class="highlight">ครกหินอ่างศิลา</span> ซึ่งเป็นผลิตภัณฑ์ที่มีชื่อเสียง
        ของตำบลอ่างศิลา จังหวัดชลบุรี

        `,
      en: `
          We are recognized as the <span class="highlight">biggest</span> and <span class="highlight">oldest</span> Angsila’s mortar manufacturer
        `

    },
    intro: {
      th: `บริษัท รุ่งเรืองศิลาทิพย์ จำกัด เริ่มต้นธุรกิจด้วย ครกหินอ่างศิลา ซึ่งเป็นผลิตภัณฑ์ที่มีชื่อเสียง
      ของตำบลอ่างศิลา จังหวัดชลบุรี และได้ขยับขยายมาสู่ธุรกิจต่างๆเกี่ยวกับหินในภายหลัง
       ด้วยประสบการณ์ และความรู้เกี่ยวกับหินที่เราสะสมมาอย่างยาวนาน ทำให้เรามีความเชี่ยวชาญ
        และฝีมือในการทำงานหิน`,
      en: `Rungreangsilathip is starting the business from mortar pestle which is really famous in Angsila Chonburi
  Rungreangsilathip is recognized as the biggest and oldest Angsila’s mortar and stone works manufacturer, especially, the Granite stone works.`
    },
    intro2: {
      th: `
  ปัจจุบัน บริษัท รุ่งเรืองศิลาทิพย์ จำกัด จึงถือได้ว่าเป็นผู้ผลิต และจำหน่ายผลิตภัณฑ์ หินแกรนิต หินแกะสลัก และครกหิน ที่ใหญ่ที่สุดในจังหวัดชลบุรี
  เรารับงานแกะสลักหินทุกประเภท โดยเฉพาะ หินแกรนิต และหินทราย เราเป็นโรงงานผลิต และจัดจำหน่ายครกหิน รวมถึงจำหน่ายหินตกแต่งหลากหลายรูปแบบ`,
      en: `The vest range of stone works that we’ve made is includes
  ,Thai mortar&pestle
  ,granite&sandstone sculpture
  ,stone for gardening decoration
  ,granite tile,Landscape paving stone
  `
    },
    servicetitle: {
      en: `The vest range of stone works that we’ve made is inclueds`,
      th: `ชนิดการแกะสลักหิน ที่เราทำ`
    },
    services:
      [{
        name: {
          th: `ครกหิน โม่หิน`,
          en: `Thai mortar&pestle`
        },
        img: "assets/images/aboutus/intro-left.jpg"
      },{
        name: {
          th: `ประติมากรรม และหินตกแต่ง งานแกะสลักจากหินแกรนิต หินทราย`,
          en: `Granite sandstone sculpture`
        },
        img: "assets/images/aboutus/slide1.jpg"
      },{
        name: {
          th: `หินปูพื้น`,
          en: `Granite tile, Landscape paving stone`
        },
        img: "assets/images/aboutus/services-3.jpg"
      }],
    end:{
      th:`
      ถ้างานหินที่อยู่บนเว็บไซต์ไม่ตรงตามความต้องการของท่านหรือมีข้อสงสัยใดๆ สามารถติดต่อสอบถามมาทางบริษัท ตามช่องทางติดต่อทางด้านล่าง ทางเรายินดีให้คำแนะนำและปรึกษา
      `,
      en:`Please contact us for more
      information, we’ll be really glad to assist you.`
    },
    serviceHead1:{
      th:`บริการงานแกะสลักหิน`,
      en:`Stone carving service`,
    },
    serviceDesc1:{
        th:`รับออกแบบ และแกะสลักตามแบบโดยทีมช่างผู้เชี่ยวชาญด้วยประสบการณ์กว่า 10 ปี ไม่ว่าจะเป็นหินแกรนิต หินอ่อน รูปแบบต่างๆ เช่น บ้านเลขที่ ป้ายอัฐิ ป้ายบริษัท ป้ายโรงเรียน งานประติมากรรมต่างๆ ฯลฯ `,
        en:`Designing and carving Granite stone, sand stone by our more than 10 years experienced professional sculptor team.`

    },
    serviceHead2:{
      th:`บริการส่งสินค้า และติดตั้งงานแกะสลัก`,
      en:`Delivery and installation work service`

    },
    serviceDesc2:{
      th:`เรามีบริการรถขนส่งสินค้า รถยก เพื่อใช้ในการติดตั้งงานตามสถานที่ต่างๆ ให้คุณหมดห่วงเรื่องการขนส่งสินค้าของคุณ อีกทั้งยังรับประกันการส่งสินค้าและติดตั้งทุกขั้นตอน`,
      en:`We manage delivery and installation your works every step of the way, we provide supervision and equipment to proceed safely and install your works properly in your location.`


    },
    serviceHead3:{
      th:`บริการผลิตและจัดจำหน่าย หินปูพื้นและหินตกแต่งประเภทต่างๆ`,
      en:`Manufacturing and selling Granite tile, landscape paving stone service`,


    },
    serviceDesc3:{
    th:`ผลิตและจัดจำหน่ายหินปูพื้น ปูท็อปเคาเตอร์ ฝาผนัง หรือหินขนาดสั่งตัดพิเศษเพื่อเป็นส่วนประกอบในงานต่างๆ รวมทั้งหินตกแต่งสวนหลากหลายชนิด โดยทางเราจะมีหินหลายประเภท รวมถึงหินแกรนิตไทยที่มีเนื้อหินคงทนมากกว่าหินประเภทอื่นๆ`,
    en:`We are manufacturer of granite stone tile and granite stone paving, so it’s possible to made a specific size to customer order.`


    }









    // end data
  }




}
